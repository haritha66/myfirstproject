package multiInheritanceDAY4;

class Animal
{
	public void eat()
	{
		System.out.println("All Animals eat");
	}	
}
 class Dog extends Animal
 {
	 public void bark() {
		 System.out.println("Dog barks");
	 }
 }
  class Cat extends Dog
  {
	  public void meow()
	  {
		System.out.println("meow...meow...");  
	  }
  }
 
public class MultiInheritance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Cat c1=new Cat();
      c1.eat();
      c1.bark();
      c1.meow();
	}

}

OUTPUT :

All Animals eat
Dog barks
meow...meow...
